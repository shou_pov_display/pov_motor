/*
  1. PERIOD(주기) : 4.08ms
  2. FREQ : 245.10Hz
 */

#define PWM_PIN 6
#define BRAKE_PIN 7 // High Motor On, Low  Motr Off
#define CW_PIN 5
#define FG_PIN 4

#define PWM_CHANNEL 0
#define FREQ 20000
#define RESOL 12


void setup(){  
  Serial.begin(115200);
  pinMode(BRAKE_PIN, OUTPUT);
  ledcSetup(PWM_CHANNEL, FREQ, RESOL);    
  ledcAttachPin(PWM_PIN, PWM_CHANNEL);   
 
  ledcWrite(PWM_CHANNEL, 716);
  digitalWrite(BRAKE_PIN, HIGH);

}  
void loop()
{
  delay(5000);  
  digitalWrite(BRAKE_PIN, HIGH);
  delay(5000);
  digitalWrite(BRAKE_PIN, LOW);
  
}