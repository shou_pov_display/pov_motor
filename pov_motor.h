#ifndef _POPSIGN_
#define _POPSIGN_


#if ARDUHAL_LOG_LEVEL >= ARDUHAL_LOG_LEVEL_DEBUG    // Log Level 디버그 레벨보다 높은 경우 Serial 출력
    #define PRINT_LINE Serial.println(__LINE__);
    #define PRINTF Serial.printf
    #define PRINTLN Serial.println
#else                                               //  Log Level 디버그 레벨 낮은 경우 Serial 출력하지 않음
    #define PRINT_LINE
    #define PRINTF
    #define PRINTLN
#endif

#define SPEED_100   10
#define SPEED_90    20
#define SPEED_80    30
#define SPEED_70    40
#define SPEED_60    50
#define SPEED_50    60
#define SPEED_40    70
#define SPEED_30    80
#define SPEED_20    90
#define SPEED_10    100

#endif
