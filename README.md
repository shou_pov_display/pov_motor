# POV(Persistence Of Visual)

# 잔상 디스플레이

### 중요 사항

- 앱 : Serial Bluetooth Terminal  
 반드시 앱 실행 시 위치기능을 켜야 앱이 정상동작한다.

- 모터 속도 및 방향 변경 방법  
 10 ~ 100까지 입력해서 모터의 속도 변경이 가능하다  
 r or R 입력을 통해 모터의 방향 설정이 가능하다.

### IDE Setting (LighitBoard, Weight Display . . .)

- Board - ESP32C3 Dev Module
- Upload Speed - 921600
- CPU Frequency - 160MHz
- Flash Frequency - 80MHz
- Flash Mode - QIO
- Flash Size - 4MB
- Partition Scheme - Default 4MB with spiffs(1.2MB APP/1.5MB SPIFFS)

