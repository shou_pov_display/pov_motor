/**
 * @file pov_motor.ino
 * @author jongju park(jongju0920@kakao.com)
 * @brief IDE Setting Read Me 참조 
 * @version 0.3.1
 * @date 2022-06-22
 * @copyright frontier (c) 2022
 *  
 */

#include <Arduino.h>
#include "pov_motor.h"
#include "src/board/board.h"
#include "src/board/config.h"
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include "EEPROM.h"

BLEServer *pServer = NULL;
BLECharacteristic * pTxCharacteristic;
bool deviceConnected = false;
bool oldDeviceConnected = false;
uint8_t txValue = 0;

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"



void saveRotation(uint8_t value) {
  EEPROM.write(1, value); // `speed_val`은 주소 0에 저장되므로, `rotation_val`은 주소 1에 저장합니다.
  EEPROM.commit();
}

void saveSpeed(uint8_t value) {
  EEPROM.write(0, value);
  EEPROM.commit();
}

uint8_t loadSpeed() {
  uint8_t value;
  value = EEPROM.readByte(0);
  Serial.printf("load speed val :%d\n", value);
  return value == 0 ? SPEED_60 : value;
}

uint8_t loadRotation() {
  uint8_t value = EEPROM.read(1); // `speed_val`은 주소 0에서 읽으므로, `rotation_val`은 주소 1에서 읽습니다.
  Serial.printf("load rotation val :%d\n", value);
  return value; // 기본값을 설정할 필요가 없다면 이렇게 반환합니다. 필요한 경우 여기서 조정할 수 있습니다.
}


uint8_t speed_val = SPEED_60;
uint8_t rotation_val = 0;
class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {
    
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();

      if (rxValue.length() > 0) {
        Serial.println("*********");
        Serial.print("Received Value: ");
        Serial.println(rxValue.c_str());

        char firstChar = rxValue[0]; // 첫 번째 문자를 가져옵니다.

        // 속도 조정 로직은 그대로 유지하고, 여기에 회전 방향 토글 로직을 추가합니다.
        if (firstChar == 'R' || firstChar == 'r') {
          rotation_val = !rotation_val; // 0이면 1로, 1이면 0으로 토글
          saveRotation(rotation_val); // 변경된 회전 방향을 저장
          Serial.printf("rotation_val :%d\n", rotation_val);
          motor_rotate(rotation_val);
        } else {
          // 기존 속도 조정 로직...
          speed_val = atoi(rxValue.c_str());
          if (speed_val == 10)
            speed_val = SPEED_10;
          else if (speed_val == 20)
            speed_val = SPEED_20;
          else if (speed_val == 30)
            speed_val = SPEED_30;
          else if (speed_val == 40)
            speed_val = SPEED_40;
          else if (speed_val == 50)
            speed_val = SPEED_50;
          else if (speed_val == 60)
            speed_val = SPEED_60;
          else if (speed_val == 70)
            speed_val = SPEED_70;
          else if (speed_val == 80)
            speed_val = SPEED_80;
          else if (speed_val == 90)
            speed_val = SPEED_90;
          else if (speed_val == 100)
            speed_val = SPEED_100;

          Serial.printf("speed_val :%d\n", speed_val);
          motor_drv(true, speed_val); // 현재 테스트 한 모터 속도 이 속도로 해야 테스트가 잘됨
          saveSpeed(speed_val);
        }
        

        Serial.println("*********");
      }
    }
};


#define USE_FAST_PROTOCOL
#define IR_RECEIVE_PIN    10
#include "TinyIRReceiver.hpp"


//#define SPEED_ON SPEED_30
#define SPEED_OFF 0
volatile struct TinyIRReceiverCallbackDataStruct sCallbackData;

void setup() 
{
  //Serial Begin
  Serial.begin(115200);

  if (!EEPROM.begin(1000)) {
    Serial.println("Failed to initialise EEPROM");
    Serial.println("Restarting...");
    delay(1000);
    ESP.restart();
  }
  
  // Load speed from flash
  speed_val = loadSpeed();

  // Load rotation from EEPROM
  rotation_val = loadRotation();

  motor_init();
  motor_rotate(rotation_val);
  motor_drv(true, speed_val); // 현재 테스트 한 모터 속도 이 속도로 해야 테스트가 잘됨
  load_switch_init();

  // Enables the interrupt generation on change of IR input signal
  if (!initPCIInterruptForTinyReceiver()) {
    Serial.printf("No interrupt available for pin %d\n", IR_RECEIVE_PIN); // optimized out by the compiler, if not required :-)
  }
  else Serial.printf("Ready to receive Fast IR signals at pin %d\n", IR_RECEIVE_PIN);


  // Create the BLE Device
  BLEDevice::init("pov_motor");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pTxCharacteristic = pService->createCharacteristic(
                    CHARACTERISTIC_UUID_TX,
                    BLECharacteristic::PROPERTY_NOTIFY
                  );
                      
  pTxCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic * pRxCharacteristic = pService->createCharacteristic(
                       CHARACTERISTIC_UUID_RX,
                      BLECharacteristic::PROPERTY_WRITE
                    );

  pRxCharacteristic->setCallbacks(new MyCallbacks());

  // Start the service
  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();
  Serial.println("Waiting a client connection to notify...");
}

void loop()
{
  if (sCallbackData.justWritten) 
  {
    sCallbackData.justWritten = false;
    
    switch (sCallbackData.Command){
      case IR_MOTOR_START:
        motor_drv(true, speed_val);
        Serial.printf("START\n");
        break;
      case IR_MOTOR_STOP:
        motor_drv(false, SPEED_OFF);
        Serial.printf("STOP\n");
        break;     
    }
  }
  //delay(1);
  // disconnecting
  if (!deviceConnected && oldDeviceConnected) {
      delay(500); // give the bluetooth stack the chance to get things ready
      pServer->startAdvertising(); // restart advertising
      Serial.println("start advertising");
      oldDeviceConnected = deviceConnected;
  }
  // connecting
  if (deviceConnected && !oldDeviceConnected) {
  // do stuff here on connecting
      oldDeviceConnected = deviceConnected;
  }
}

/*
 * This is the function is called if a complete command was received
 * It runs in an ISR context with interrupts enabled, so functions like delay() etc. should work here
 */
IRAM_ATTR void handleReceivedTinyIRData(uint8_t aCommand, uint8_t aFlags)
 {
  // Copy data for main loop, this is the recommended way for handling a callback :-)
  sCallbackData.Command = aCommand;
  sCallbackData.Flags = aFlags;
  sCallbackData.justWritten = true;
}
