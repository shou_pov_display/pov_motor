#include <Arduino.h>

#include "../board/board.h"

#include <VCNL3036.h>
#include <Wire.h>
//#include <IRremote.h>//IR리모컨라이브러리

// /** 
//  * @brief - 적외선 센서 체크
// **/
// void IRAM_ATTR check_ir_rx() 
// {
//   // stop motor
// 	motor_drv(false, 0);
// }


void motor_init(void)
{
  pinMode(BRAKE_PIN, OUTPUT);
  digitalWrite(BRAKE_PIN, LOW); // Low Motor Off

  pinMode(FG_PIN, INPUT);
  
  pinMode(CW_PIN, OUTPUT);

  ledcSetup(PWM_CHANNEL, FREQ, RESOL); //채널과, Hz, resolution 을 지정
  ledcAttachPin(PWM_PIN, PWM_CHANNEL); // 핀 과 채널을 매칭
  ledcWrite(PWM_CHANNEL, PWM_DUTY_50); // 
}

void motor_rotate(uint8_t rotate)
{
  if (rotate == 0)
    digitalWrite(CW_PIN, LOW); // HIgh : Motor CCW, Low : Motor CW
  else digitalWrite(CW_PIN, HIGH); // HIgh : Motor CCW, Low : Motor CW
}

void load_switch_init(void)
{
  pinMode(EN_WC_PIN, OUTPUT);
  digitalWrite(EN_WC_PIN, HIGH); // High Load Switch On
}



void motor_drv(bool onoff, int duty)
{
  if (onoff == true) {    
    digitalWrite(BRAKE_PIN, HIGH); // Motor On
    ledcWrite(PWM_CHANNEL, (int)((float)MOTOR_RESOL_12BIT * ((float)duty/100))); 
      }
  else {
    digitalWrite(BRAKE_PIN, LOW); // Motor Off
    ledcWrite(PWM_CHANNEL, 0);
      }
}

void ir_rx_init(void)
{
  pinMode(IR_RX_TOP_PIN, INPUT_PULLUP);
  pinMode(IR_RX_BOTTOM_PIN, INPUT_PULLUP);
  //attachInterrupt(digitalPinToInterrupt(IR_RX_TOP_PIN), check_ir_rx, HIGH); 

}