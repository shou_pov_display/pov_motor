#ifndef _BOARD_
#define _BOARD_


/*
    모터 관련 설정
  1. PERIOD(주기) : 4.08ms
  2. FREQ : 245.10Hz
 */

#define PWM_PIN 5
#define BRAKE_PIN 19 // High Motor On, Low  Motr Off
#define CW_PIN 4
#define FG_PIN 4

#define PWM_CHANNEL 2
#define FREQ 5000
#define RESOL 12
#define MOTOR_RESOL_12BIT 4096

#define PWM_DUTY_10 (int)((MOTOR_RESOL_12BIT * 0.1)-1)
#define PWM_DUTY_20 (int)((MOTOR_RESOL_12BIT * 0.2)-1)
#define PWM_DUTY_50 (int)((MOTOR_RESOL_12BIT * 0.5)-1)
#define PWM_DUTY_100 (int)((MOTOR_RESOL_12BIT * 1)-1)



/*
  적외선 수신 센서 관련
*/

#define IR_RX_TOP_PIN 7
#define IR_RX_BOTTOM_PIN 10

#define IR_MOTOR_STOP   0xA1 // Stop
#define IR_MOTOR_START  0xA2 // Start 


/*
  Load Switch 관련
*/

#define EN_WC_PIN 6

void load_switch_init(void);
void motor_init(void);
void motor_rotate(uint8_t rotate);
void motor_drv(bool onoff, int duty);
void ir_rx_init(void);

#endif
